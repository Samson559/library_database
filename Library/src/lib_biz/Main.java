
package lib_biz;

import java.util.Scanner;

/**
 * Class for interacting/testing DB_Accessor
 * @author noaker
 *
 */
public class Main {
	public static void main(String[] args) {
		Boolean go = true;
		DB_accessor dber = null;
		try
		{
			dber = new DB_accessor(System.out);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.err.println ("Cannot connect to database server");
		}
		while(go)
		{
			System.out.println("enter a functionality tag or -h for help");
			Scanner s = new Scanner(System.in);
			String cmd = s.nextLine();


			if(cmd.equals("-h"))
			{
				System.out.println(dber.printHelp());
			}
			else if(cmd.equals("RegUser"))
			{
				System.out.println("Enter '`' delimited user data like this:\n"
						+ "uname`fullname`addr`phone`email");
				cmd = s.nextLine();
				String[] subArgs = cmd.split("`");
				System.out.println("your card no. is: "+dber.RegUser(subArgs[0],subArgs[1],subArgs[2],subArgs[3],subArgs[4])); 
			}
			else if(cmd.equals("NewBook"))
			{
				System.out.println("Enter '`' delimited book data like this:\n"
						+"(ISBN`title`authors`publisher`pubyear`format`subject`summary)" );
				cmd = s.nextLine();
				
				String[] subArgs = cmd.split("`");
				dber.NewBook(subArgs[0], 
						subArgs[1], 
						subArgs[2], 
						subArgs[3], 
						subArgs[4], 
						subArgs[5], 
						subArgs[6], 
						subArgs[7]);
			}
			else if(cmd.startsWith("StockBook"))
			{
				System.out.println("Enter '`' delimited inventory data like this:\n"
						+"(ISBN`location`quantity)" );
				cmd = s.nextLine();
				String[] subArgs = cmd.split("`");
				dber.stockBooks(subArgs[0], subArgs[1], subArgs[2]);
			}
			else if(cmd.startsWith("CheckOut"))
			{
				System.out.println("Enter '`' delimited Checkout data like this:\n"
						+"(User cid`ISBN)" );
				cmd = s.nextLine();
				String[] subArgs = cmd.split("`");
				dber.CheckOut(subArgs[0], subArgs[1]);
			}
			else if(cmd.startsWith("Return"))
			{
				System.out.println("Enter '`' delimited return data like this:\n"
						+"(ISBN`User cid`copyID)" );
				cmd = s.nextLine();
				String[] subArgs = cmd.split("`");
				dber.ReturnBook(subArgs[0], subArgs[1], subArgs[2]);
			}
			else if(cmd.startsWith("LoseBook"))
			{
				System.out.println("Enter '`' delimited Lost Book data like this:\n"
						+"(ISBN`User cid`copyID)" );
				cmd = s.nextLine();
				String[] subArgs = cmd.split("`");
				dber.LoseBook(subArgs[0], subArgs[1], subArgs[2]);
			}
			else if(cmd.startsWith("ReqStock"))
			{
				System.out.println("Enter '`' delimited request data like this:\n"
						+"(ISBN`User cid)" );
				cmd = s.nextLine();
				String[] subArgs = cmd.split("`");
				dber.RequestStock(subArgs[0], subArgs[1]);
			}
			else if(cmd.startsWith("RvwBook"))
			{
				System.out.println("Enter '`' delimited rvw data like this:\n"
						+"(ISBN`User cid`opinion text`score [1-10])" );
				cmd = s.nextLine();
				String[] subArgs = cmd.split("`");
				dber.ReviewBook(subArgs[0], subArgs[1], subArgs[2], subArgs[3]);
			}
			else if(cmd.startsWith("UserRecord"))
			{
				System.out.println("Enter a card id to look up\n");
				cmd = s.nextLine();
				System.out.println(dber.UserReport(cmd));
			}
			else if(cmd.startsWith("LateBooks"))
			{
				System.out.println(dber.GetLateBooks());
			}
			else if(cmd.equals("SearchBook"))
			{
				System.out.println("Search only books in inventory? (y/n)");
				cmd = s.nextLine();
				if(!(cmd.equals("y")||cmd.equals("n")))
				{
					System.out.println("Enter y or n");
					break;
				}
				boolean inventory = cmd.equals("y");
				System.out.println("Enter authors delimited by a '|'");
				cmd = s.nextLine();
				String[] Authors = cmd.split("\\|");
				System.out.println("Enter Title (inexact titles or keywords will suffice)");
				String Title = s.nextLine();
				System.out.println("Enter subject (inexact subject or keywords will suffice)");
				String subject= s.nextLine();
				System.out.println("How do you want your result sorted?");
				System.out.println("enter one of: pubyear, AVG(score), popularity");
				System.out.println("where pubyear is the year of publication,\n"
						+ " AVG(score) is the average review score,\n"
						+ " popularity means number of times checked out");
				String orderby =s.nextLine();
				System.out.println(dber.SearchBook(inventory,Authors,Title,subject,orderby));
			}
			else if(cmd.equals("BookRecord"))
			{
				System.out.println("Type the ISBN for the book you want a record of");
				cmd = s.nextLine();
				System.out.println(dber.BookRecord(cmd));
			}
			else if(cmd.equals("BookStats"))
			{
				
				System.out.println(dber.bookStats());
			}
			else if(cmd.equals("UserStats"))
			{
				
				System.out.println(dber.UserStats());
			}
			else if(cmd.startsWith("Waitlist"))
			{
				System.out.println("Enter '`' delimited Waitlist data like this:\n"
						+"(card id`ISBN)");
				cmd = s.nextLine();
				String[] subArgs = cmd.split("`");
				dber.waitlist(subArgs[0], subArgs[1]);
			}
			else if(cmd.equals("-q"))
			{
				go = false;
			}
			System.out.println("DONE");
		}
		dber.close();
		System.out.println("Bye");
	}
	
}

