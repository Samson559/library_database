package lib_biz;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.PrintStream;
import java.sql.*;
/**
 * Class for accessing Dylan's Database on the georgia server
 * provides methods for library data transactions
 * @author noaker
 *
 */
public class DB_accessor {
	private String sql = null;
	private Connection con = null;
	private long prev_cid = 0;
	private Statement stmt = null;
	private Date today = new Date();
	private SimpleDateFormat fmt_as_sql_date = new SimpleDateFormat("yyyy/MM/dd");
	private final long ONE_MONTH = (31*24*60*60*1000L);
	private PrintStream out;
	private PreparedStatement pstmt = null;
	/**
	 * stateVal
	 *An enumeration of possible bit values for the state of book copies in inventory.
	 *Wrapping these values in an enum reduces confusion by adding a layer of abstraction.
	 */
	public enum stateval
	{
		SHELVED,		//00
		RETURNED,		//01
		CHECKED_OUT,	//10
		LOST;			//11
	}
	/**
	 * Connect
	 * <a> Connects the user to my Database<a>
	 * @throws Exception e
	 * if it can't connect
	 */
	public void connect() throws Exception
	{
		out.println("Example for CS5530");

		try
		{
			String userName = "cs5530u11";
			String password = "mqp6gtar";
			String url = "jdbc:mysql://georgia.eng.utah.edu/cs5530db11";
			Class.forName ("com.mysql.jdbc.Driver").newInstance ();
			con = DriverManager.getConnection (url, userName, password);
			out.println ("Database connection established");
			stmt=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

		}
		catch(Exception e)
		{
			System.err.println("CONNECTION ERR");
			throw e;
		}
	}
	/**
	 * 
	 */
	public DB_accessor(PrintStream _out)
	{
		out = _out;
		out.println("Connecting to Lib database");
		try {
			connect();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Closes connection to my database
	 */
	public void close() 
	{
		if (con != null)
		{
			try
			{
				con.close ();
				out.println ("Database connection terminated");
			}
			catch (Exception e) 
			{ 
				e.printStackTrace(); 
			}
		}

	}
	/**
	 * 
	 * @param _uname
	 * @param _addr
	 * @param _fullname
	 * @param _phone
	 * @param _email
	 * @return
	 */
	public long RegUser(String _uname,
			String _fullname,
			String _addr,
			String _phone,
			String _email)
	{
		
		//find the highest card id and start with that.
		try {
			ResultSet r = stmt.executeQuery("SELECT MAX(`cid`) AS top FROM `UserData`");
			r.first();
			prev_cid = r.getLong(1);
			prev_cid++;
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			pstmt = con.prepareStatement("INSERT INTO `UserData` (`uname`,`cid`,`fullname`,`addr`,`phone`,`email`)"
					+ " VALUES ( ?,?,?,?,?,?)");
			pstmt.setString(1, _uname);
			pstmt.setLong(2, prev_cid);
			pstmt.setString(3,_fullname);
			pstmt.setString(4,_addr);
			pstmt.setString(5,_phone);
			pstmt.setString(6,_email);
			pstmt.execute();
			return prev_cid;
		} catch (SQLException e1) {
			out.println("in Reg user");
			if(e1.getMessage().startsWith("Duplicate"))
				System.err.println("User entry already exists");
			System.err.println("Failed to register user!");
			e1.printStackTrace();
			return -1;
		}
/*
		sql="INSERT INTO `UserData` (`uname`,`cid`,`fullname`,`addr`,`phone`,`email`) "
				+ "VALUES"+ "("
				+"\""+_uname+"\""+","
				+prev_cid+","
				+"\""+_fullname+"\""+","
				+"\""+_addr+"\""+","
				+"\""+_phone+"\""+","
				+"\""+_email+"\""
				+")";
		try {
			stmt.executeUpdate(sql);
			return prev_cid;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
			return -1;
		}
		BAD BAD BAD */
	}
	public void NewBook(String _ISBN,
			String _Title,
			String _Authors,
			String _publisher,
			String _pubyear,
			String _Format,
			String _subject,
			String _summary)
	{
		try {
			pstmt = con.prepareStatement("INSERT INTO `BookData` (`ISBN`,`title`,`publisher`,`pubyear`,`format`,`subject`,`summary`) " +
					"VALUES (?,?,?,?,?,?,?)");
			pstmt.setString(1,_ISBN);
			pstmt.setString(2,_Title);
			pstmt.setString(3,_publisher);
			pstmt.setString(4,_pubyear);
			pstmt.setString(5,_Format);
			pstmt.setString(6,_subject);
			pstmt.setString(7,_summary);
			pstmt.execute();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			if(e1.getMessage().startsWith("Duplicate"))
				System.err.println("Book entry already exists");
			System.err.println("Failed to add book!");
			e1.printStackTrace();
		}
		//add the book to the BookData table
		/*
		sql = "INSERT INTO `BookData` (`ISBN`,`title`,`publisher`,`pubyear`,`format`,`subject`,`summary`)"
				+ "VALUES"+ "("
				+"\""+_ISBN+"\","
				+"\""+_Title+"\""+","
				+"\""+_publisher+"\""+","
				+_pubyear+","
				+"\""+_Format+"\""+","
				+"\""+_subject+"\""+","
				+"\'"+_summary+"\'"
				+")";
		try 
		{
			stmt.execute(sql);
		} 
		catch (SQLException e) 
		{
			if(e.getMessage().startsWith("Duplicate"))
				System.err.println("Book entry already exists");
			System.err.println("Failed to add book!");
			e.printStackTrace();
		} STUUUUUUUUUUUUUUPIIIIIIIIID!!!!!
		*/
		//split authors into tokens
		try {
			pstmt = con.prepareStatement("INSERT INTO `Authors`(`author`,`ISBN`)" +
					" VALUES(?,?)");
			con.setAutoCommit(false);
			String[] authors = _Authors.split("\\|");
			//add them (author, isbn) to authors
			for(int i = 0; i<authors.length; i++)
			{
				pstmt.setString(1, authors[i]);
				pstmt.setString(2,_ISBN);
				//sql+="("+"\""+authors[i]+"\""+",\""+_ISBN+"\")";
				//comma separate each value to add
				//if(i<authors.length-1)
					//sql+=",";
				pstmt.addBatch();
				if((i+1)%1000==0)
				{
					pstmt.executeBatch();
				}
			}
			pstmt.executeBatch();
			con.commit();
			con.setAutoCommit(true);
		} catch (SQLException e1) {
			e1.printStackTrace();
			if(e1.getMessage().startsWith("Duplicate"))
				System.err.println("Author entry already exists");
			System.err.println("Book insert no worky...");
			e1.printStackTrace();
		}
		//add the whole batch!
		/*
		try 

		{
			stmt.execute(sql);
		} 
		catch (SQLException e)
		{
		
		}
		*/
	}
	/**
	 * printHelp
	 * Returns a string which tells the user how to use the tester
	 * @return
	 */
	public String printHelp()
	{
		String toReturn = "";
		toReturn +="RegUser: Prompts you for information to add a new user to the database \n "
				+ "NewBook: Prompts you for information to add a new book to the database \n"
				+ "Author list should be delimited with a \"|\" \n"
				+ "UserRecord: prompts you for a username. If the user is in the DB, their user record is printed \n"
				+ "StockBook: prompts you for an ISBN and a quantity to add to the library inventory\n"
				+ "LateBooks: Lists all overdue books\n"
				+ "Return: prompts you for information required to return a book\n"
				+ "ReqStock: Prompts you for information required to request the library to stock a book\n"
				+ "RvwBook: Prompts you for information required to review a book\n";

		return toReturn;
		// TODO Auto-generated method stub

	}
	/**
	 *
	 * @param _ISBN: ISBN FK to book data
	 * @param _location: Where is it in the library?
	 * @param _qty: How many book copies to add.
	 * _state:Copy status 00 -> on shelf, 01 -> returned, 10 ->checked out, 11->lost
	 */
	public void stockBooks(String _ISBN,String _location,String _qty)
	{
		//first, we need to see how many copies (if any) we have in the library.
		int topCopy=0;
		int qty = Integer.parseInt(_qty);
		try {
			pstmt = con.prepareStatement("SELECT MAX(`copyid`) FROM Inventory Where ISBN = ?");
			pstmt.setString(1, _ISBN);
			ResultSet r = pstmt.executeQuery();
			r.first();
			topCopy = r.getInt(1)+1;
			if(r.wasNull()) //might not be anything there.
				topCopy = 0;
			
		} catch (SQLException e2) {
			System.err.println("Failed to find highest copyid");
			e2.printStackTrace();
		}
		//insert copies based on current inventory.
		try 
		{
			con.setAutoCommit(false);
			pstmt = con.prepareStatement("INSERT INTO `Inventory` (`ISBN`,`copyid`,`location`,`state`) VALUES(?,?,?,?)");
			for(int i = 1; i<=qty; i++)
			{
				pstmt.setString(1, _ISBN);
				pstmt.setInt(2, topCopy++);
				pstmt.setString(3, _location);
				pstmt.setByte(4, (byte) stateval.SHELVED.ordinal());
				pstmt.addBatch();
				if((i+1)%1000==0)
				{
					pstmt.executeBatch();
				}
				
			}
			pstmt.executeBatch();
			con.commit();
			con.setAutoCommit(true);
		} 
		catch (SQLException e1)
		{
			out.println("Failed to stock book");
			e1.printStackTrace();
		}
	}
	public void CheckOut(String _cid,String _ISBN)
	{
		//which copy should we check out? 
		//How about the highest one we can find on the shelf.
		int copyid = 0;
		
		sql= "SELECT MAX(copyid) "
				+ "FROM Inventory "
				+ "Where ISBN = ? AND (state = 00 or state = 1)";
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, _ISBN);
			ResultSet r=pstmt.executeQuery();
			r.first();
			copyid = r.getInt(1);
		} catch (SQLException e) {
			System.err.println("couldn't find copy");
			e.printStackTrace();
			return;
		}
		//check if there is a waitlist.
		/*
		String[] waitlist = get_waitlist(_ISBN);
		if(waitlist!=null)
		{
			//is the first user in the waitlist the same user trying to check out this book copy?
			if(waitlist[0]==_cid)
			{
				//good! let him/her check it out by first de-waitlisting him/her
				deWaitlist(_ISBN, _cid);
			}
			else
			{
				//user was not at back of the waitlist.If he/she isn't in the waitlist, add him/her.
				//commence bad coding practice!!!
				boolean contains = false;
				for(int i = 0; i<waitlist.length;i++)
				{
					if(waitlist[i]==_cid)
					{
						contains = true;
						break;
					}
				}
				if(!contains)
					waitlist(_cid, _ISBN);
				//leave the method
				
				return;
			}
		}
		*/
		//allow checkout
		sql = "INSERT IGNORE INTO `BooksOut` (`ISBN`,`cid`,`copyid`,`outdate`,`duedate`,`retdate`)" +
				" VALUES(?,?,?,?,?,?)";
		//date manipulation borrowed from Mykong @ http://www.mkyong.com/java/java-how-to-get-current-date-time-date-and-calender/
		Date today = new Date();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");
		String outdate = fmt.format(today);
		today.setTime(today.getTime()+ONE_MONTH);
		String duedate = fmt.format(today);
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, _ISBN);
			pstmt.setString(2, _cid);
			pstmt.setInt(3,copyid);
			pstmt.setString(4,outdate);
			pstmt.setString(5,duedate);
			pstmt.setNull(6,Types.DATE);
			pstmt.execute();
		} catch (SQLException e) {
			System.err.println("couldn't check it out");
			e.printStackTrace();
		}
		//set state of inventory copy to checked out
		sql ="update Inventory "
				+ "set state = ? "
				+ "Where ISBN = ? AND copyid = ?"; 
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1,stateval.CHECKED_OUT.ordinal());
			pstmt.setString(2,_ISBN);
			pstmt.setInt(3,copyid);
			pstmt.execute();
		} catch (SQLException e) {
			System.err.println("Failed to update state of book in inventory");
			e.printStackTrace();
		}
		
	}
	/**
	 * Returns the book
	 * @param _ISBN
	 * @param _cid
	 * @param _copyid
	 */
	public void ReturnBook(String _ISBN,String _cid, String _copyid)
	{
		//set the return date in BooksOut
		java.sql.Date retdate= new java.sql.Date(today.getTime());
		sql = "update BooksOut "
				+"set retdate = ? Where ISBN = ? AND copyid = ? AND cid = ?" ;
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setDate(1, retdate);
			pstmt.setString(2, _ISBN);
			pstmt.setInt(3, Integer.parseInt(_copyid));
			pstmt.setLong(4, Long.parseLong(_cid));
			pstmt.execute();
		} catch (SQLException e) {
			System.err.println("Couldn't update return date for checked out book");
			e.printStackTrace();
		}
		catch (NumberFormatException nfe)
		{
			System.err.println("Bad number format");
			nfe.printStackTrace();
		}
		//set the state of the book in inventory
		sql ="update Inventory "
				+ "set state = ? "
				+ "Where ISBN = ? AND copyid = ?"; 
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1,stateval.RETURNED.ordinal());
			pstmt.setString(2,_ISBN);
			pstmt.setInt(3,Integer.parseInt(_copyid));
			pstmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Failed to update state of book in inventory");
			e.printStackTrace();
		}
	}
	/**
	 * lets users request to stok things
	 */
	public void RequestStock(String _ISBN, String _cid)
	{
		//get today's date
		today.setTime(today.getTime());
		String reqdate = fmt_as_sql_date.format(today);
		//add the request to the request table
		
		sql = "INSERT INTO `BookReq` (`ISBN`,`cid`,`reqdate`) VALUES "
				+ "(?,?,?)";
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, _ISBN);
			pstmt.setLong(2, Long.parseLong(_cid));
			pstmt.setString(3, reqdate);
			pstmt.execute();
		} catch (SQLException e) {
			System.err.println("failed to request specified book");
			e.printStackTrace();
		}
	}
	/**
	 * Lets users review a book
	 * @param _ISBN
	 * @param _cid
	 * @param _opinion
	 * @param _score
	 */
	public void ReviewBook(String _ISBN, String _cid, String _opinion, String _score)
	{
		today.setTime(today.getTime());
		String rvwdate = fmt_as_sql_date.format(today);
		sql = "INSERT INTO `Reviews` (`ISBN`,`cid`,`opinion`,`rvwdate`,`score`) VALUES"
				+"(?,?,?,?,?)";
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, _ISBN);
			pstmt.setLong(2,Long.parseLong(_cid));
			pstmt.setString(3, _opinion);
			pstmt.setString(4, rvwdate);
			pstmt.setInt(5, Integer.parseInt(_score));
			pstmt.execute();
		} catch (SQLException e) {
			System.err.println("Failed to review book.");
			e.printStackTrace();
		}
		
	}
	/**
	 * User Report
	 * generates a user report of the form
	 * all personal data (name, username, address, etc.)
	 * Full history of book checked out in the past (ISBN, title, dates of checkout and return)
	 * Full list of books lost by the user.
	 * Full list of books requested for future checkout.
	 * Full list of reviews the have written for books (score, text,
	 * @param _cid	:the card id of the user you want info about
	 * @return printable user report
	 */
	public String UserReport(String _cid)
	{
		String toReturn = "User Report for userName ";
		String data = "";
		sql = "SELECT * FROM UserData Where cid = ?";
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, Long.parseLong(_cid));
			ResultSet r = pstmt.executeQuery();
			r.first();
			data += r.getString(1) + "\n";
			data += "Card Id: "+r.getString(2)+ "\n";
			data += "Full Name: "+r.getString(3)+ "\n";
			data += "Address: "+r.getString(4)+ "\n";
			data += "Phone #: "+r.getString(5)+ "\n";
			data += "Email: "+r.getString(6)+ "\n";
		} catch (SQLException e) {
			System.err.println("Failed to retrieve user data while generating user report");
			e.printStackTrace();
		}
		//-------------------------Get Checkout List -----------------------------------
		toReturn+=data + "Checkout History (ISBN,title,outdate,retdate):\n";
		sql = "Select BookData.ISBN,BookData.title,outdate,retdate "
				+ "From BookData JOIN BooksOut "
				+ "ON BookData.ISBN = BooksOut.ISBN AND BooksOut.cid = ?";
		data = "";
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, Long.parseLong(_cid));
			ResultSet r = pstmt.executeQuery();
			r.beforeFirst();
			while(r.next())
			{
				data+=",("+r.getString(1)+",";
				data+= r.getString(2)+",";
				data+= r.getString(3)+",";
				data+= r.getString(4)+")\n";
			}
		}
		catch (SQLException e)
		{
			System.err.println("Failed to Check out data while generating user report");
			e.printStackTrace();
		}
		toReturn+=data+"Lost Books\n";
		data= "";
		//----------------------------Get Lost Books-------------------------------------
		sql = "Select BookData.ISBN,title "
			+ "From  BookData Join "
				+ "( "
					+ "SELECT Inventory.ISBN "
					+ "From BooksOut JOIN Inventory "
					+ "ON BooksOut.ISBN =Inventory.ISBN "
					+ "AND BooksOut.cid = ? "
					+ "AND BooksOut.`copyid` = Inventory.`copyid` "
					+ "AND Inventory.state = "+stateval.LOST.ordinal()+" "
					+ ")AS BI "
			+ "ON BookData.ISBN = BI.ISBN";
		try 
		{
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, Long.parseLong(_cid));
			out.println(pstmt.toString());
			ResultSet r = pstmt.executeQuery();
			r.beforeFirst();
			while(r.next())
			{
				data+=",("+r.getString(1)+",";
				data+= r.getString(2)+")\n";
			}
		} 
		catch (SQLException e)
		{
			System.err.println("Couldn't get data on lost books");
			e.printStackTrace();
		}
		//------------------------------get book requests-------------------------------
		toReturn+=data+"Book Requests\n";
		data= "";
		sql = "Select BookData.ISBN,title "
				+ "From BookData Join BookReq "
				+ "ON BookData.ISBN = BookReq.ISBN AND BookReq.cid = ?";
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, Long.parseLong(_cid));
			ResultSet r = pstmt.executeQuery();
			r.beforeFirst();
			while(r.next())
			{
				data+=",("+r.getString(1)+",";
				data+= r.getString(2)+")\n";
			}
		} catch (SQLException e) {
			System.err.println("Couldn't get data on book requests");
			e.printStackTrace();
		}
		//------------------------------get book reviews-------------------------------
		toReturn+=data+"Book Reviews\n";
		data= "";
		sql = "Select BookData.ISBN,title,score,opinion "
				+ "From BookData Join Reviews "
				+ "ON BookData.ISBN = Reviews.ISBN AND Reviews.cid = ?";
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, Long.parseLong(_cid));
			ResultSet r = pstmt.executeQuery();
			r.beforeFirst();
			while(r.next())
			{
				data+=",("+r.getString(1)+",";
				data+= r.getString(2)+",";
				data+= r.getString(3)+",";
				data+= r.getString(4)+")\n";
			}
		} catch (SQLException e) {
			System.err.println("Couldn't get data on book reviews");
			e.printStackTrace();
		}
		toReturn+=data;
		return toReturn;
	}
	public void LoseBook(String _ISBN, String _cid, String _copyid)
	{
		//set the return date in BooksOut (It's the day they lost it)
				String retdate = fmt_as_sql_date.format(today);
				sql = "update BooksOut "
						+"set retdate = ?"
						+"Where ISBN = ? AND copyid =? AND cid =?";
				try {
					pstmt = con.prepareStatement(sql);
					pstmt.setString(1, retdate);
					pstmt.setString(2,_ISBN);
					pstmt.setInt(3, Integer.parseInt(_copyid));
					pstmt.setLong(4, Long.parseLong(_cid));
					pstmt.execute();
				} catch (SQLException e) {
					System.err.println("Couldn't update Lost date for Lost book");
					e.printStackTrace();
				}
				//set the state of the book in inventory
				//set the state of the book in inventory
				sql ="update Inventory "
						+ "set state = ? "
						+ "Where ISBN = ? AND copyid = ?"; 
				try {
					pstmt = con.prepareStatement(sql);
					pstmt.setInt(1,stateval.LOST.ordinal());
					pstmt.setString(2,_ISBN);
					pstmt.setInt(3,Integer.parseInt(_copyid));
					pstmt.executeUpdate();
				} catch (SQLException e) {
					System.err.println("Failed to update state of book in inventory");
					e.printStackTrace();
				}
	}
	public String GetLateBooks() {
		// TODO Auto-generated method stub
		String toReturn = "(title,ISBN,duedate,uname,phone,email)\n";
		sql = "Select title,BookData.ISBN,duedate,uname,phone,email "
				+ "From BookData Join "
				+ "( "
				+ "Select ISBN,duedate,uname,phone,email "
				+ "From BooksOut JOIN UserData "
				+ "On BooksOut.cid = UserData.cid "
				+ "Where retdate>duedate OR (retdate=null & duedate<curdate()) "
				+ ") AS copydata "
				+ "On BookData.ISBN = copydata.ISBN";
		try {
			pstmt = con.prepareStatement(sql);
			ResultSet r = pstmt.executeQuery();
			r.beforeFirst();
			while(r.next())
			{
				toReturn+=",("+r.getString(1)+",";
				toReturn+= r.getString(2)+",";
				toReturn+= r.getString(3)+",";
				toReturn+= r.getString(4)+",";
				toReturn+= r.getString(5)+",";
				toReturn+= r.getString(6)+")\n";
			}
		} catch (SQLException e) {
			System.err.println("Couldn't get data on book reviews");
			e.printStackTrace();
		}
		return toReturn;
	}
	/**
	 * 
	 * @param subArgs :Elelment 0: y/n on search inventory
	 * 			:element1-authors,title words, 
	 * @return
	 */
	public String SearchBook(boolean _stocked,String[] _authors,String _title, String _subject, String _ordering) 
	{
		String data= "";
		//adaptively build query based on params
		String sql = "Select BookData.ISBN,title,author,subject";
		String join_clause = "FROM(BookData JOIN Authors";
		String onclause ="ON BookData.ISBN = Authors.ISBN";
		String GroupBy = "GROUP BY (author)";
		String having = "HAVING(";
		//build having clause
		//add author conditions
		for(int i = 0; i<_authors.length; i++)
		{
			//author LIKE "%abcd%" or
			//At end of array
			if(i==_authors.length-1)
				having+="author LIKE "+"\"%"+_authors[i]+"%\"";
			else
				having+="author LIKE "+"\"%"+_authors[i]+"%\""+" OR ";
			
		}
		//add title conditions
		if(!_title.equals(""))
		{
			if(_authors.length>0)
				having+=" AND ";
			having+="title LIKE "+"\"%"+_title+"%\"";
		}
		if(!_subject.equals(""))
		{
			if(_authors.length>0||!_title.equals(null))
				having+=" AND ";
			having+="subject LIKE "+"\"%"+_subject+"%\"";
		}
		having+=") ";
		//build sql
		if(_stocked)
		{
			join_clause +=" JOIN Inventory";
			onclause += " AND BookData.ISBN = Inventory.ISBN AND Inventory.state = 1 OR Inventory.state = 2";
		}
		if(_ordering.equals("AVG(score)"))
		{
			join_clause += " JOIN Reviews";
			onclause += " AND BookData.ISBN = Reviews.ISBN";
			sql+=",AVG(score) AS avgscr "
					+join_clause+" "
					+onclause+") "
					+having
					+"order by(avgscr) DESC";
		}
		else if(_ordering.equals("popularity"))
		{
			join_clause += " JOIN BooksOut";
			onclause += " AND BookData.ISBN = BooksOut.ISBN";
			sql="SELECT *, Count(ISBN) AS ckout FROM ("+sql+" "
					+join_clause+" "
					+onclause+") "
					+having
					+") AS stupidsql "
					+GroupBy+" "
					+"order by(ckout) DESC";
		}
		else if(_ordering.equals("pubyear"))
		{
			//do nothing fancy to join and on in this case. Book Data has this info
			sql+=",pubyear "
			+join_clause+" "
			+onclause+") "
			+having
			+"order by(pubyear) DESC";
		}
		//run query to get data
		try {
			pstmt = con.prepareStatement(sql);
			ResultSet r = pstmt.executeQuery();
			r.beforeFirst();
			while(r.next())
			{
				//If the ISBN hasn't popped up yet, add the data to the string
				if(!data.contains(r.getString(1)))
				{
					data+="ISBN:"+r.getString(1)+"\n";
					data+= "Title:"+r.getString(2)+"\n";
					data+= "Authors:<"+r.getString(3)+">\n";
					data+= "Subject:"+r.getString(4)+"\n";
					data+= "Year Published: "+r.getString(5)+")\n\n";
				}
				//else, it's likely we are just seeing the same book with a diff author
				//add the author to that existing book in data
				else
				{
					//find the previous occurrence of this book
					int index = data.indexOf(r.getString(1));
					//find the end of its author list
					index =data.indexOf(">",index);
					//break it in half and add the author
					data = data.substring(0, index)+r.getString(3)+", "+data.substring(index, data.length());
					//not particularly efficient but it produces the expected results
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		data.replaceAll("<+|>+", "");
		return data;
	}
	/**
	 * BookRecord: prints a record of the given book(ISBN) in the form
	 * BookData:
	 * ISBN,Title,authors,subject,publisher,pubyear,format,summary
	 * Copy list:
	 * Copyid,location
	 * Checkouts:
	 * copyid,cid,uname,outdate-retdate(NOW) if null
	 * Reviews: 
	 * Avg score: XX
	 * rvws
	 * @param _ISBN
	 */
	public String BookRecord(String _ISBN)
	{
		String toreturn = "BookData:\n";
		String data= "";
		sql = "Select BookData.ISBN,title,author,subject,publisher,pubyear,format,summary "
				+"FROM BookData JOIN Authors ON BookData.ISBN = Authors.ISBN "
				+"WHERE BookData.ISBN = ?";
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, _ISBN);
			ResultSet r= pstmt.executeQuery();
			r.first();
			data+=r.getString(1);
			data+=r.getString(2);
			data+=r.getString(3);
			//get every author. all the rows are the same book
			while(r.next())
			{
			data+=", "+r.getString(3);
			}
			r.previous();
			data+=r.getString(4);
			data+=r.getString(5);
			data+=r.getString(6);
			data+=r.getString(7);
			data+=r.getString(8);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		toreturn+=data+"\nCopyList:\n";
		sql="select copyid,location "
				+ "From Inventory "
				+ "Where ISBN = ?";
		data = "";
		
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, _ISBN);
			ResultSet r = pstmt.executeQuery();
			r.beforeFirst();
			while(r.next())
			{
				data+=r.getString(1)+", ";
				data+=r.getString(2)+"\n";
			}
			toreturn+=data;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Get the people who checked this book out before
		data = "";
		toreturn+= "\nPast Users:\n";
		sql = " select uname,UserData.cid,outdate,retdate "
				+ "from UserData Join BooksOut ON UserData.cid = BooksOut.cid "
				+ "Where ISBN = ?";

		try 
		{
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, _ISBN);
			ResultSet r = pstmt.executeQuery();
			while(r.next())
			{
				data+=r.getString(1)+": ";
				data+="Checked out during "+r.getString(3)+"-->";
				r.getString(4);
				if(r.wasNull())
					data+="NOW";
				else
					data+=r.getString(4)+"\n";
			}
			toreturn+=data;
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		data = "";
		toreturn+="\nReviews:\n";
		sql = "SELECT uname,score,opinion,Reviews.ISBN "
				+ "From Reviews JOIN UserData on UserData.cid = Reviews.cid "
				+ "Where Reviews.ISBN = ?";
		try 
		{
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, _ISBN);
			ResultSet r = pstmt.executeQuery();
			while(r.next())
			{
				data+=r.getString(1)+": ";
				data+="Score: "+r.getString(2)+" Review: ";
				r.getString(3);
				if(!r.wasNull())
					data+=r.getString(3)+"\n";
			}
			toreturn+=data;
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return toreturn;
	}
	public String bookStats()
	{
		//most popular by checkout
		String data = "\nMost Popular By Checkouts\n";
		sql = "select title,BookData.ISBN,ckout from " +
				"(BookData join " +
				"(Select ISBN,count(ISBN) as ckout " +
				"From BooksOut " +
				"Group by(ISBN) " +
				")As g1 " +
				"ON BookData.ISBN = g1.ISBN " +
				") " +
				"order by (ckout) Desc";
		try {
			pstmt = con.prepareStatement(sql);
			ResultSet r = pstmt.executeQuery();
			r.beforeFirst();
			while(r.next())
			{
				data+=r.getString(1)+", ";
				data+=r.getLong(2)+", ";
				data+=r.getInt(3)+"\n";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		data+="\nMost Requested\n";
		sql = "select title,BookData.ISBN,rvwmost "
				+ "From "
				+ "(BookData Join "
				+ "(Select ISBN,count(ISBN) as rvwmost "
				+ " From Reviews Group By(`ISBN`))AS g1 "
				+ "ON BookData.ISBN = g1.ISBN) "
				+ "order by (rvwmost) Desc;";
		try {
			pstmt = con.prepareStatement(sql);
			ResultSet r = pstmt.executeQuery();
			r.beforeFirst();
			while(r.next())
			{
				data+=r.getString(1)+", ";
				data+=r.getLong(2)+", ";
				data+=r.getInt(3)+"\n";
			}
		} catch (SQLException e) {
			out.println(pstmt.toString());
			e.printStackTrace();
		}
		data+="\nMost Lost\n";
		sql = "select title,BookData.ISBN,lostMost "
				+ "From "
				+ "(BookData Join "
				+ "(Select ISBN,state,count(ISBN) as lostmost "
				+ " From Inventory Group By(`ISBN`)Having State = 3)AS g1 "
				+ "ON BookData.ISBN = g1.ISBN) "
				+ "order by (lostmost) Desc;";
		try {
			pstmt = con.prepareStatement(sql);
			ResultSet r = pstmt.executeQuery();
			r.beforeFirst();
			while(r.next())
			{
				data+=r.getString(1)+", ";
				data+=r.getLong(2)+", ";
				data+=r.getInt(3)+"\n";
			}
		} catch (SQLException e) {
			out.println(pstmt.toString());
			e.printStackTrace();
		}
		data+="\nMost Popular Authors\n";
		sql = "Select j1.author, Count(ISBN) as ckout "
				+ "From ( "
				+ " Select Authors.ISBN,author "
				+ "From Authors Join BooksOut On Authors.ISBN =BooksOut.ISBN "
				+ ") As j1 "
				+ "group by (j1.author) "
				+ "order by (ckout) Desc";
		try {
			pstmt = con.prepareStatement(sql);
			ResultSet r = pstmt.executeQuery();
			r.beforeFirst();
			while(r.next())
			{
				data+=r.getString(1)+", ";
				data+=r.getInt(2)+"\n";
			}
		} catch (SQLException e) {
			out.println(pstmt.toString());
			e.printStackTrace();
		}
		return data;
	}
	public String UserStats()
	{
		String data = "Top Users by Checkout:\n";
		sql = "select uname,UserData.cid,fullname,ckout" +
				" from (" +
				"UserData join( " +
				"Select ISBN,cid,count(ISBN) as ckout " +
				"From BooksOut " +
				"Group by(cid) " +
				")As g1 " +
				"ON g1.cid = UserData.cid) " +
				"order by (ckout) Desc;";
		try {
			pstmt = con.prepareStatement(sql);
			ResultSet r = pstmt.executeQuery();
			r.beforeFirst();
			while(r.next())
			{
				data+=r.getString(1)+", ";
				data+=r.getInt(2)+", ";
				data+=r.getString(3)+", ";
				data+=r.getInt(4)+"\n";
			}
		} catch (SQLException e) {
			out.println(pstmt.toString());
			e.printStackTrace();
		}
		data += "\nTop critics\n";
		sql = "select uname,UserData.cid,fullname,rvwmost " +
				"from ( " +
				"UserData join " +
				"(Select ISBN,cid,count(ISBN) as rvwmost " +
				"From Reviews " +
				"Group by(cid)" +
				" )As g1 " +
				"ON g1.cid = UserData.cid) " +
				"order by (rvwmost) Desc;";
		try {
			pstmt = con.prepareStatement(sql);
			ResultSet r = pstmt.executeQuery();
			r.beforeFirst();
			while(r.next())
			{
				data+=r.getString(1)+", ";
				data+=r.getInt(2)+", ";
				data+=r.getString(3)+", ";
				data+=r.getInt(4)+"\n";
			}
		} catch (SQLException e) {
			out.println(pstmt.toString());
			e.printStackTrace();
		}
		data += "\nTop Book-Losers\n";
		sql = "select uname,UserData.cid,fullname,losemost from " +
				"( UserData join " +
				"(select BooksOut.ISBN,cid,state,losemost " +
				"From BooksOut join ( " +
				"Select ISBN,state,count(ISBN) as losemost " +
				"From Inventory " +
				"Group By(ISBN) " +
				"Having (state = 3) " +
				")As g1 On BooksOut.ISBN = g1.ISBN" +
				" )As j1 " +
				"ON j1.cid = UserData.cid ) " +
				"order by (losemost) Desc;";		
		try {
			pstmt = con.prepareStatement(sql);
			ResultSet r = pstmt.executeQuery();
			r.beforeFirst();
			while(r.next())
			{
				data+=r.getString(1)+", ";
				data+=r.getInt(2)+", ";
				data+=r.getString(3)+", ";
				data+=r.getInt(4)+"\n";
			}
		} catch (SQLException e) {
			out.println(pstmt.toString());
			e.printStackTrace();
		}
		return data;
	}
	/**
	 * Waitlist: Add specified user to waitlist for specified book.
	 *  Too be used in conjunction with Checkout
	 * @param _cid
	 * @param _ISBN
	 */
	public void waitlist(String _cid, String _ISBN)
	{
		//init sql. The insert IGNORE is justified as a way to keep a user from adding themselves
		//multiple times for the same book.
		sql = "INSERT Ignore INTO `Waitlist` (`ISBN`,`cid`,`dateadded`) VALUES (?,?,?)";
		try {
			//do query
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, _ISBN);
			pstmt.setLong(2, Long.parseLong(_cid));
			String dateadded = fmt_as_sql_date.format(today);
			pstmt.setString(3, dateadded);
			pstmt.execute();
		} catch (SQLException e) {
			if(e.getMessage().startsWith("Duplicate"))
				System.err.println("Waitlist entry already exists");
			e.printStackTrace();
		}
		//leave
	}
	/**
	 * Get_waitlist: gets the user's Card Id's from the waitlist for a book specified by ISBN ordered by ascending date-added
	 * returns null if no waitlist exists for book specified by ISBN.
	 * @param _ISBN : ISBN of book which we want to get the waitlist of.
	 * @return Returns a string array containing all of the cids paired with matching isbn
	 */
	private String[] get_waitlist(String _ISBN)
	{
		String[] toreturn = null;
		
		//init sql
		sql = "Select * from Waitlist where ISBN = ? order by (dateadded) ASC";
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1,_ISBN);
			ResultSet r = pstmt.executeQuery();
			//get size of result set for array
			if(r.last())
				toreturn = new String[r.getRow()];
			else
				return null;
			r.beforeFirst();
			//process resultset
			while(r.next())
			{
				toreturn[r.getRow()] =r.getString(2);
			}
		} catch (SQLException e) {
			out.println(pstmt.toString());
			e.printStackTrace();
		}	
				//return
		return toreturn;
		
	}
	/**
	 * remove the oldest entry in a waitlist for a book
	 * @param _cid
	 * @param _ISBN
	 */
	private void deWaitlist(String _ISBN,String _cid)
	{
		sql = "Delete from Waitlist where ISBN = ? AND cid = ?";
		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, _ISBN);
			pstmt.setLong(2, Long.parseLong(_cid));
			pstmt.execute();
		} catch (SQLException e) {
			out.println(pstmt.toString());			
			e.printStackTrace();
		}
	}
}
